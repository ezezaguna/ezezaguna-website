#!/bin/sh

_yn() {
	stty -icanon
	printf '> '
	local input="$(dd bs=1 count=1 2>/dev/null)"
	stty icanon
	printf '\n'
	case "$input" in
		y|Y) return 0 ;;
		*) return 1 ;;
	esac
}

_bak() {
	local ext=".bak"
	local i=1
	while [ -f "${1}${ext}" ]; do
		i=$(expr $i + 1)
		ext=".bak${i}"
	done
	mv -v "$1" "${1}${ext}"
}

[ x"$1" = "x" ] && echo "post or a video?" && exit 1
[ x"$2" = "x" ] && echo "what category?" && exit 1
[ x"$3" = "x" ] && echo "what filename?" && exit 1

case "$1" in
	p*) _SECTION="posts" ;;
	v*) _SECTION="videos" ;;
	*)
		[ -d "$1" ] && _SECTION="$1" && break
		echo "section ${1}"
		echo "is that correct?"
		_yn || exit 2
		_SECTION="$1"
		;;
esac

_NEWPOST="${_SECTION}/${2}/$(date '+%Y-%m-%d')-${3}/index.md"

if [ -f "./content/$_NEWPOST" ]; then
	echo "post $_NEWPOST already exists"
	echo "backup?"
	if _yn; then
		_bak "./content/$_NEWPOST"
	else
		exit 0
	fi
fi

hugo new "$_NEWPOST"
sed -i 's/^categories:.*$/categories: "'"$2"'"/' "./content/$_NEWPOST"
sed -i 's/^channels:.*$/channels: "'"$2"'"/' "./content/$_NEWPOST"

which cowsay > /dev/null && _COOLECHO=cowsay || _COOLECHO="echo"
which lolcat > /dev/null && $_COOLECHO "do you want me to open it in a text editor?" | lolcat || $_COOLECHO "do you want me to open it in a text editor?"
if ! which "$EDITOR" > /dev/null ; then
	which nano > /dev/null && EDITOR=nano
	which vi > /dev/null && EDITOR=vi
	which nvim > /dev/null && EDITOR=nvim
	which kak > /dev/null && EDITOR=kak
fi

_yn && exec "$EDITOR" "./content/${_NEWPOST}" || echo "bye."

