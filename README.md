# ezezaguna.gitlab.io

My website at [ezezaguna.poo.li](https://ezezaguna.poo.li) built with [Hugo](https://gohugo.io/)

## Directories

* `content/` contains all pages
	* `about/` - about page
	* `posts/` - blog posts
	* `videos/` - videos
* `data/comments/` contains all your comments :heart:

## Contributing

Contributions are more than welcome!

## License

[Creative Commons Attribution Share Alike 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
