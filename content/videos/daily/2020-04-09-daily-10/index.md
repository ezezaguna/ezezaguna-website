---
title: "daily #10"
date: "2020-04-09T21:50:05+02:00"
featured_image: "featured_image.jpg"
featured_image: "webcam black noise"
channels: "daily"
tags:
 - sound
video:
  peertube: "f502155b-9f8e-4f49-8e47-57416af09ca3"
---

when i enable the integrated laptop webcam there's that cool sound on the
microphone.

<!--more-->

i'm wondering how are you supposed to make video calls with this thing.
