---
title: "daily #2"
subtitle: "a print"
date: "2020-04-01T00:00:00+00:00"
featured_image: "featured_image.jpg"
featured_image_alt: "a printer with a printed letter a"
channels: "daily"
tags:
 - printing
 - a printer
video:
  peertube: "ac8b46ca-73fa-4cc6-848b-cbf1aaa8ddc6"
audio:
  soundcloud: "a-print-daily-2"
---
