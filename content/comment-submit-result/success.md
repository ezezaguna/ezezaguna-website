---
title: "you can close this tab"
modal_title: "very happy"
modal_color: "#388e3c"
robots_noindex: true
---

your comment has been submitted and will be published once it has been approved.

[see the merge request you've generated](https://gitlab.com/ezezaguna/ezezaguna-website/merge_requests)
